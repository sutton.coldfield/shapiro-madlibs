# 1. Load the MadLibs words from a JSON file.
import json
with open('words.json') as jsonfile:
    words = json.load(jsonfile)
adverbs    = words[ 'adverbs' ]
verbs      = words[ 'verbs' ]
adjectives = words[ 'adjectives' ]
person     = words[ 'persons' ]
noun       = words[ 'nouns' ]

# 2. Randomly select words to fill in a SAVAGE template.
import random

for i in range( 10 ):
    print(f'Watch Ben Shapiro {random.choice(adverbs).upper()} {random.choice(verbs).upper()} this {random.choice(adjectives)} {random.choice(person)} in a {random.choice(noun).upper()} of {random.choice(noun).upper()}.')

